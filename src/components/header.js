import styles from './header.module.css'

function Header() {
  return (
        <div className={styles['container1']}>
                <a href="#contact">UNB</a>
                <a href="#about">BRIDGES</a>  
                <a href="#about">REC</a>  
                <div className={styles['main']}><a class="active" href="#home"><img className={styles['img-header']} alt="logo" src='/logo.png'></img></a></div>
                <a href="#about">TALKS</a>
                <a href="#about">STORE</a>
                <a href="#about">CONTACT</a>
        </div>
  );
}

export default Header;