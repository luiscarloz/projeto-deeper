import styles from './slider.module.css'

function Slider() {
  return (
    <div className={styles["container"]}>
      <img src="./slider.jpg"></img>
    </div>
  );
}

export default Slider;