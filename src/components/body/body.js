import styles from './body.module.css'
import Slider from './slider.js'

function Body() {
  return (
    <div className={styles['container']}>
         <Slider />
    </div>
  );
}

export default Body;